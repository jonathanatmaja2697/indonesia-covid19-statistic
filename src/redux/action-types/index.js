const SET_SHOW_LOADING = "@set-show-loading";
const SET_DISMISS_LOADING = "@set-dismiss-loading";
const SET_PROVINCE_LIST = "@get-province-list";
const SET_NEWS_LIST = "@set-news-list";

export {
  SET_PROVINCE_LIST,
  SET_SHOW_LOADING,
  SET_DISMISS_LOADING,
  SET_NEWS_LIST,
};
