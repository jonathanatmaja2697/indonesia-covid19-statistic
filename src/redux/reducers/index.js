import {
  SET_PROVINCE_LIST,
  SET_SHOW_LOADING,
  SET_DISMISS_LOADING,
  SET_NEWS_LIST
} from "../action-types";

const initialState = {
  provinceList: [],
  loading: false,
  news: []
};

export const reducerActions = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_PROVINCE_LIST:
      return { ...state, provinceList: payload };
    case SET_SHOW_LOADING:
      return { ...state, loading: true };
    case SET_DISMISS_LOADING:
      return { ...state, loading: false };
      case SET_NEWS_LIST:
      return { ...state, loading: false };
    default:
      return state;
  }
};
