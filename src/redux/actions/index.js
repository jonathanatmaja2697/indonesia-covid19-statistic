import axios from "axios";
import {
  SET_PROVINCE_LIST,
  SET_NEWS_LIST,
  SET_SHOW_LOADING,
  SET_DISMISS_LOADING,
} from "../action-types";

const baseUrl = process.env.NEXT_PUBLIC_BASE_URL;
const newsUrl = process.env.NEXT_PUBLIC_NEWS_URL;
const newsApiKey = process.env.NEXT_PUBLIC_NEWS_API;

const setProvinceList = (payload) => ({
  type: SET_PROVINCE_LIST,
  payload,
});

const setNewsList = (payload) => ({
  type: SET_NEWS_LIST,
  payload,
});

const showLoading = (payload) => ({
  type: SET_SHOW_LOADING,
  payload,
});

const dismissLoading = (payload) => ({
  type: SET_DISMISS_LOADING,
  payload,
});

const getProvinceList = (showLoading, dismissLoading) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(dismissLoading());

    axios
      .get(baseUrl + "/prov_list.json")
      .then((res) => {
        dispatch(setProvinceList(res.data.list_data));
        resolve(res.data.list_data);
      })
      .catch((err) => {
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const getNewsList = (showLoading, dismissLoading) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    console.log(newsUrl + newsApiKey);
    axios
      .get(newsUrl + newsApiKey)
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

export { getProvinceList, getNewsList, showLoading, dismissLoading };
