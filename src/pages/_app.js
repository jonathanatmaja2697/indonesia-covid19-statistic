import "bootstrap/dist/css/bootstrap.css";
import { createWrapper } from "next-redux-wrapper";
import React from "react";
import { Provider, useSelector } from "react-redux";
import store from "../../src/redux";
import { Loading } from "../components";
import { AnimateSharedLayout } from "framer-motion";
import '../styles/globals.css';

function MyApp({ Component, pageProps }) {
  const { loading } = useSelector((state) => state.data);
  

  return (
    <Provider store={store}>
      <AnimateSharedLayout>
        <Loading loading={loading} />
        <Component {...pageProps} />
      </AnimateSharedLayout>
    </Provider>
  );
}
const makestore = () => store;
const wrapper = createWrapper(makestore);

export default wrapper.withRedux(MyApp);
