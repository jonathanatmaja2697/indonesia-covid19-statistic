import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Card, Container, Content, Gap, Header } from "../components";
import { hp } from "../functions";
import {
  dismissLoading,
  getNewsList,
  getProvinceList,
  showLoading,
} from "../redux/actions";
import { List, ListItem } from "@mui/material";
import moment from "moment";

const Home = () => {
  const dispatch = useDispatch();
  const { provinceList } = useSelector((state) => state.data);
  useEffect(() => {
    dispatch(getProvinceList(showLoading, dismissLoading));
    dispatch(getNewsList(showLoading, dismissLoading));
  }, []);

  const renderCardHeader = () => {
    return (
      <div>
        <div style={{ fontFamily: "Montserrat-Bold" }}>Top Cases -</div>
        <div>{moment().format("dddd, DD MMMM YYYY")}</div>
      </div>
    );
  };

  return (
    <Container>
      <Header />
      <Content>
        <img src="/assets/indonesia.png" />
        <Gap height={hp(10)} />
        <Card headerTitle={renderCardHeader()}>
          <List>
            {provinceList.map((v, i) => {
              if (i < 5) {
                return (
                  <ListItem disablePadding>
                    <div
                      style={{
                        display: "flex",
                        flex: 1,
                        justifyContent: "space-between",
                      }}
                    >
                      <div>
                        <p>
                          {i + 1}. {v.key}
                        </p>
                      </div>
                      <div>
                        <p>{v.doc_count}</p>
                      </div>
                    </div>
                  </ListItem>
                );
              }
            })}
          </List>
        </Card>
      </Content>
    </Container>
  );
};
export default Home;

const styles = {};
