import React, { useState } from "react";
import { Container, Content, Footer, Gap, Header } from "../../components";
import { Button, Select, TextField } from "@mui/material";
import { hp, wp } from "../../functions";

const Register = () => {
  const onSubmit = (e) => {
    e.preventDefault();
    const { name } = e.target;
  };
  return (
    <Container>
      <Header />
      <Content centered>
        <form onSubmit={onSubmit}>
          <TextField variant="outlined" label="Nama" size="small" name="name" />
          <Gap height={20} />
          <Button variant="contained" type="submit">
            Klik aku mas
          </Button>
        </form>
      </Content>
    </Container>
  );
};

export default Register;

const styles = {
  input: (height) => ({
    width: "100%",
    height: wp(height),
  }),
};
