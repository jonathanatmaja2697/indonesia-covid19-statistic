import { Card, CardContent, Divider } from "@mui/material";
import React from "react";

const MyCard = ({ headerTitle = "", children }) => {
  return (
    <Card sx={{ minWidth: 275 }} variant="outlined">
      {headerTitle ? (
        <>
          <div style={styles.cardHeader}>{headerTitle}</div>
          <Divider variant="middle" />
        </>
      ) : null}
      <CardContent>{children}</CardContent>
    </Card>
  );
};

export default MyCard;

const styles = {
  card: (width = "100%", height) => ({
    width: "100%",
    height,
    backgroundColor: "red",
    borderWidth: 1,
    borderColor: "black",
  }),
  cardHeader: {
    padding: 18,
  },
};
