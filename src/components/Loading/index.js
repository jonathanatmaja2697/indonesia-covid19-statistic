import React from "react";
import { ScaleLoader } from "react-spinners";
import { Backdrop } from "@mui/material";

const Loading = ({ loading }) => {
  return (
    <Backdrop
      open={loading}
      transitionDuration={{ enter: 500, exit: 500 }}
      sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}
    >
      <ScaleLoader />
    </Backdrop>
  );
};

export default Loading;
