import { AppBar, Container, Toolbar } from "@mui/material";
import React, { useState } from "react";
import { Gap } from "..";
import { hp, wp } from "../../functions";

const Header = () => {
  return (
    <AppBar position="static" color="transparent" sx={styles.container(hp(10))}>
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <div>
            <img src="/assets/covid.png" style={styles.icon(wp(12))} />
          </div>
          <div style={styles.textContainer}>
            <p style={styles.text}>Indonesia Covid-19 Cases</p>
          </div>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default Header;

const styles = {
  icon: (width) => ({
    width,
    height: "auto",
    resizeMode: "cover",
  }),
  container: (height) => ({ height, justifyContent: "center" }),
  textContainer: {
    display: "flex",
    flex: 1,
    justifyContent: "flex-end",
  },
  text: {
    marginTop: "auto",
    marginBottom: "auto",
    fontFamily: "Retroica",
  },
};
